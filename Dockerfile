FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/sap-hcp-mms-connector

WORKDIR /home/node/sap-hcp-mms-connector

RUN npm install pm2@2.6.1 -g

CMD pm2-docker --json app.yml