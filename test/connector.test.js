'use strict'

const amqp = require('amqplib')

let _channel = null
let _conn = null
let app = null
let timestamp = Date.now()

describe('HCP MMS Connector Test', () => {
  before('init', () => {
    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      app = require('../app')
      app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should send data to third party client', function (done) {
      this.timeout(15000)

      let data = {
        timestamp: timestamp,
        data: JSON.stringify({
          temp: 20
        }),
        rkhDeviceInfo: {
          device: '9cbc4ef2-656c-4950-a584-cf6cd84ecb1f',
        }
      }

      _channel.sendToQueue('ip.hcpmms', new Buffer(JSON.stringify(data)))
      setTimeout(done, 10000)
    })
  })
})
